package br.com.desafioconcrete.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


/**
 * Classe utilitaria para obter status ou dados do aparelho
 * @author Fernando
 */
public class DeviceUtils {

	
	/**
	 * Verifica conexão
	 * @param context
	 * @return true = Conectado.
	 */
	public static boolean isNetConnection(Context context){
		ConnectivityManager connetivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if(connetivity == null){
			return false;
		}else{
			NetworkInfo[] info = connetivity.getAllNetworkInfo();
		    if(info != null){
		    	for (int i = 0; i < info.length; i++) {
					if(info[i].getState() == NetworkInfo.State.CONNECTED){
						return true;
					}
				}
		    }
		}
		
		return false;
	}
	

	

	
	
}
