package br.com.desafioconcrete.model.response;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Fernando on 30/08/2016.
 */
public class ResponseListRepository implements Serializable {
 private int total_count;
 private List<ItemRepository> items;
 public int getTotal_count() {
  return total_count;
 }

 public void setTotal_count(int total_count) {
  this.total_count = total_count;
 }

 public List<ItemRepository> getItems() {
  return items;
 }

 public void setItems(List<ItemRepository> items) {
  this.items = items;
 }


}
