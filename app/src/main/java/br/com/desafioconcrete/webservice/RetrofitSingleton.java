package br.com.desafioconcrete.webservice;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by fernandogomes on 04/03/16.
 */
public class RetrofitSingleton {


    private static RetrofitSingleton instance;
    private Retrofit retrofit;


    private RetrofitSingleton(){
        Gson gson = new GsonBuilder().registerTypeAdapter(Deserializer.class, new Deserializer()).create();
        retrofit = new Retrofit
                .Builder()
                .baseUrl(WebService.DOMAIN_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public static Retrofit getInstance() {
        if(instance == null){
            instance = new RetrofitSingleton();
        }
        return instance.retrofit;
    }

}
