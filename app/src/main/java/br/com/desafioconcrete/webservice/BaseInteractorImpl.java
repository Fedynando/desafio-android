package br.com.desafioconcrete.webservice;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.net.URLEncoder;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Fednando on 03/01/2016.
 */
public class BaseInteractorImpl{
    private Retrofit retrofit;

    protected GitApi getService(){
        retrofit = RetrofitSingleton.getInstance();
        return retrofit.create(GitApi.class);
    }

    public Error getError(Response response, Context context){
        try {
            Converter<ResponseBody, Error> errorConverter =  retrofit.responseBodyConverter(Error.class, new Annotation[0]);
            return errorConverter.convert(response.errorBody());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

}
