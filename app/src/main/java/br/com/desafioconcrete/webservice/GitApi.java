package br.com.desafioconcrete.webservice;


import java.util.List;

import br.com.desafioconcrete.model.response.ResponseListPullRequest;
import br.com.desafioconcrete.model.response.ResponseListRepository;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by fernandogomes on 09/06/16.
 */
public interface GitApi {

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("search/repositories?q=language:Java&sort=stars")
    Call<ResponseListRepository> listRepository( @Query("page") int page);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("repos/{owner}/{repos}/pulls")
    Call<List<ResponseListPullRequest>> listPullRequest(@Path("owner") String owner, @Path("repos") String repos);




}
