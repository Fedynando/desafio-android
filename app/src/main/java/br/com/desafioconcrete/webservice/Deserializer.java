package br.com.desafioconcrete.webservice;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by fernandogomes on 05/11/15.
 */
public class Deserializer implements JsonDeserializer<Object> {
    @Override
    public Object deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonElement action = json.getAsJsonObject();

        if(json.getAsJsonObject().get("error") != null){
            action = json.getAsJsonObject().get("error");
        }

        return (new Gson().fromJson(action, typeOfT));

    }
}