package br.com.desafioconcrete.webservice;

import com.google.gson.annotations.SerializedName;

/**
 * Created by fernandogomes on 17/06/16.
 */
public class Error {

    private int code;
    private String message;

    public Error(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public long getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
