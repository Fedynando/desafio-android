package br.com.desafioconcrete.activity.listRepository;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.desafioconcrete.R;
import br.com.desafioconcrete.databinding.AdapterListRepositoryBinding;
import br.com.desafioconcrete.databinding.FooterBinding;
import br.com.desafioconcrete.model.response.ItemRepository;

/**
 * Created by Fernando on 01/09/2016.
 */
public class AdapterListRepository extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private OnItemSelectedListener listener;
    private List<ItemRepository> mListItems;
    private final int ITEM = 0;
    private final int FOOTER = 1;
    private boolean isLoad;

    public AdapterListRepository(Context context, List<ItemRepository> mListItems, OnItemSelectedListener listener) {
        this.context = context;
        this.listener = listener;
        this.mListItems = mListItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == ITEM){
            AdapterListRepositoryBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                    R.layout.adapter_list_repository, parent, false);
            return new ViewHolder(binding);
        }else {
            FooterBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                    R.layout.footer, parent, false);
            return new ViewHolderFooter(binding);
        }
    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.bindItem(position);
        }else if(holder instanceof ViewHolderFooter){
            ViewHolderFooter viewHolderFooter = (ViewHolderFooter) holder;
            viewHolderFooter.bindItem();
        }
    }

    @Override
    public int getItemCount() {
        if(mListItems != null){
            return mListItems.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public AdapterListRepositoryBinding binding;

        public ViewHolder(AdapterListRepositoryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setOnClickListener(this);
        }

        public void bindItem(int position){
            ItemRepository item = mListItems.get(position);
            binding.setItem(item);
            Picasso.with(context).load(item.getOwner().getAvatarUrl()).placeholder(R.drawable.ic_image_avatar).into(binding.imageAvatar);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            listener.onItemSelected(mListItems.get(position));
        }
    }

    public class ViewHolderFooter extends RecyclerView.ViewHolder{
        public FooterBinding footerBinding;

        public ViewHolderFooter(FooterBinding binding){
            super(binding.getRoot());
            footerBinding = binding;

        }

        public void bindItem(){
            if(isLoad){
                footerBinding.progressBar.setVisibility(View.VISIBLE);
            }else{
                footerBinding.progressBar.setVisibility(View.GONE);
            }
        }
    }

     interface OnItemSelectedListener {
        void onItemSelected(ItemRepository Item);
    }

    @Override
    public int getItemViewType(int position) {
        if(position == mListItems.size() -1){
            return FOOTER;
        }
        return ITEM;
    }

    protected void setLoad(boolean isLoad){
        this.isLoad = isLoad;
    }

}
