package br.com.desafioconcrete.activity.pullRequest;

import android.content.Context;

import java.util.List;

import br.com.desafioconcrete.R;
import br.com.desafioconcrete.model.response.ResponseListPullRequest;
import br.com.desafioconcrete.model.response.ResponseListRepository;
import br.com.desafioconcrete.webservice.BaseInteractorImpl;
import br.com.desafioconcrete.webservice.Error;
import br.com.desafioconcrete.webservice.GitApi;
import br.com.desafioconcrete.webservice.WebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by fernandogomes on 10/06/16.
 */
public class ListPullRequestInteractorImpl extends BaseInteractorImpl implements ListPullRequestInteractor {

    @Override
    public void listPullRequest(final Context context, String repos, String owner, final OnListPullRequestFinishedListener listener) {
        GitApi gitApi = getService();

        Call<List<ResponseListPullRequest>> call = gitApi.listPullRequest(owner, repos);
        call.enqueue(new Callback<List<ResponseListPullRequest>>() {
            @Override
            public void onResponse(Call<List<ResponseListPullRequest>> call, Response<List<ResponseListPullRequest>> response) {
                if(!response.isSuccessful() && response.code() != 200){
                    Error error = getError(response, context);
                    if(error != null && error.getMessage() != null){
                        listener.onError(error.getMessage());
                        return;
                    }
                    listener.onError(context.getResources().getString(R.string.message_error_unknown));
                    return;
                }

                listener.onListPullRequestSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<ResponseListPullRequest>> call, Throwable t) {
                listener.onError(context.getResources().getString(R.string.message_error_unknown));
            }
        });
    }
}
