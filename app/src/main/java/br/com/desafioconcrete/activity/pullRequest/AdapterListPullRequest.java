package br.com.desafioconcrete.activity.pullRequest;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.desafioconcrete.R;
import br.com.desafioconcrete.databinding.AdapterListPullRequestBinding;
import br.com.desafioconcrete.model.response.ResponseListPullRequest;

/**
 * Created by Fernando on 01/09/2016.
 */
public class AdapterListPullRequest extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private OnItemSelectedListener listener;
    private List<ResponseListPullRequest> mListPullRequest;


    public AdapterListPullRequest(Context context, List<ResponseListPullRequest> mListPullRequest, OnItemSelectedListener listener) {
        this.context = context;
        this.listener = listener;
        this.mListPullRequest = mListPullRequest;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        AdapterListPullRequestBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.adapter_list_pull_request, parent, false);

        ViewHolder viewHolder = new ViewHolder(binding);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.bindItem(position);
        }
    }

    @Override
    public int getItemCount() {
        return mListPullRequest.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public AdapterListPullRequestBinding binding;

        public ViewHolder(AdapterListPullRequestBinding binding) {
            super(binding.getRoot());
            binding.getRoot().setOnClickListener(this);
            this.binding = binding;
        }

        public void bindItem(int position){
            ResponseListPullRequest responseListPullRequest = mListPullRequest.get(position);
            binding.setItem(responseListPullRequest);
            Picasso.with(context).load(responseListPullRequest.getUser().getAvatarUrl()).placeholder(R.drawable.ic_image_avatar).into(binding.imageAvatar);
        }

        @Override
        public void onClick(View v) {
            ResponseListPullRequest responseListPullRequest = mListPullRequest.get(getAdapterPosition());
            listener.onItemSelected(responseListPullRequest);
        }
    }

    interface OnItemSelectedListener {
        void onItemSelected(ResponseListPullRequest responseListPullRequest);
    }


}
