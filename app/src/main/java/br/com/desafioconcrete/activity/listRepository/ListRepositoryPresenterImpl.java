/*
 *
 *  * Copyright (C) 2014 Kanamobi.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package br.com.desafioconcrete.activity.listRepository;

import android.app.Activity;
import android.content.Context;
import br.com.desafioconcrete.base.BaseView;
import br.com.desafioconcrete.model.response.ResponseListRepository;
import br.com.desafioconcrete.utils.DeviceUtils;

public class ListRepositoryPresenterImpl implements ListRepositoryPresenter, OnListRepositoryFinishedListener {

    private ListRepositoryView baseView;
    private ListRepositoryInteractor interactor;
    private Context context;

    public ListRepositoryPresenterImpl(Context context, ListRepositoryView baseView) {
        this.baseView = baseView;
        this.context = context;
        this.interactor = new ListRepositoryInteractorImpl();
    }


    @Override
    public void onListRepositorySuccess(final ResponseListRepository responseListRepository) {
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                baseView.hideProgress();
                baseView.onListRepositorySucess(responseListRepository);
            }
        });
    }


    @Override
    public void onError(final String errorMessage) {
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                baseView.hideProgress();
                baseView.onError(errorMessage);
            }
        });
    }

    @Override
    public void listRepository(int page, boolean showProgress) {
        if(!DeviceUtils.isNetConnection(context)){
            baseView.onConnectionFailed();
            return;
        }

        if(showProgress){
            baseView.showProgress(BaseView.ProgressType.PROGRESS_DIALOG);
        }
        interactor.listRepository(context, page, this);
    }
}
