package br.com.desafioconcrete.activity.listRepository;

import br.com.desafioconcrete.base.BaseView;
import br.com.desafioconcrete.model.response.ResponseListRepository;

/**
 * Created by Fernando on 30/08/2016.
 */
public interface ListRepositoryView extends BaseView{
    void onListRepositorySucess(ResponseListRepository responseListRepository);
    void onError(String error);
}
