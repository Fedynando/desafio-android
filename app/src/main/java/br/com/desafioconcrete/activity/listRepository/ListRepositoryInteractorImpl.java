package br.com.desafioconcrete.activity.listRepository;

import android.content.Context;

import br.com.desafioconcrete.R;
import br.com.desafioconcrete.model.response.ResponseListRepository;
import br.com.desafioconcrete.webservice.BaseInteractorImpl;
import br.com.desafioconcrete.webservice.Error;
import br.com.desafioconcrete.webservice.GitApi;
import br.com.desafioconcrete.webservice.WebService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by fernandogomes on 10/06/16.
 */
public class ListRepositoryInteractorImpl extends BaseInteractorImpl implements ListRepositoryInteractor {

    @Override
    public void listRepository(final Context context, int page, final OnListRepositoryFinishedListener listener) {
        GitApi gitApi = getService();

        Call<ResponseListRepository> call = gitApi.listRepository(page);
        call.enqueue(new Callback<ResponseListRepository>() {
            @Override
            public void onResponse(Call<ResponseListRepository> call, Response<ResponseListRepository> response) {
                if(!response.isSuccessful() && response.code() != 200){
                       Error error = getError(response, context);
                       if(error != null && error.getMessage() != null){
                           listener.onError(error.getMessage());
                           return;
                       }
                       listener.onError(context.getResources().getString(R.string.message_error_unknown));
                       return;
                     }

                 listener.onListRepositorySuccess(response.body());

            }

            @Override
            public void onFailure(Call<ResponseListRepository> call, Throwable t) {
                listener.onError(context.getResources().getString(R.string.message_error_unknown));
            }
        });
    }
}
