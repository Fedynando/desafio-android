package br.com.desafioconcrete.activity.pullRequest;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import br.com.desafioconcrete.R;
import br.com.desafioconcrete.activity.listRepository.ListRepositoryActivity;
import br.com.desafioconcrete.base.BaseActivity;
import br.com.desafioconcrete.databinding.ActivityListBinding;
import br.com.desafioconcrete.model.response.ItemRepository;
import br.com.desafioconcrete.model.response.ResponseListPullRequest;

public class ListPullRequestActivity extends BaseActivity implements ListPullRequestView, AdapterListPullRequest.OnItemSelectedListener {

    private ListPullRequestPresenter mPresenter;
    private ActivityListBinding binding;
    private AdapterListPullRequest adapter;
    private List<ResponseListPullRequest> mListPullRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUI();
        initData();

    }

    @Override
    public void initUI() {
        super.initUI();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_list);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_navigation_arrow_back);

        binding.recyclerView.setHasFixedSize(false);
        binding.recyclerView.setItemAnimator(new DefaultItemAnimator());
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void initData() {
        final ItemRepository itemRepository = (ItemRepository) getIntent().getExtras().get(ListRepositoryActivity.BUNDLE_ITEM);
        mListPullRequest = new ArrayList<>();
        adapter = new AdapterListPullRequest(this, mListPullRequest, this);
        binding.recyclerView.setAdapter(adapter);

        mPresenter = new ListPullRequestPresenterImpl(this, this);
        mPresenter.listPullRequest(itemRepository.getName(), itemRepository.getOwner().getLogin());

        binding.swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                binding.swipeContainer.setRefreshing(false);
                mPresenter.listPullRequest(itemRepository.getName(), itemRepository.getOwner().getLogin());
            }
        });
    }

    @Override
    public void onListPullRequestSucess(List<ResponseListPullRequest> listPullRequests) {
       mListPullRequest.clear();
       mListPullRequest.addAll(listPullRequests);
       adapter.notifyDataSetChanged();
    }

    @Override
    public void onError(String error) {
        toast(error);
    }

    @Override
    public void showProgress(ProgressType type) {
        onLoadingStart();
    }

    @Override
    public void hideProgress() {
        onLoadingFinish();
        if(binding.swipeContainer.isRefreshing()){
            binding.swipeContainer.setRefreshing(false);
        }
    }

    @Override
    public void onConnectionFailed() {
        toast(getString(R.string.not_connect));
    }

    @Override
    public void onItemSelected(ResponseListPullRequest responseListPullRequest) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(responseListPullRequest.getHtmlUrl()));
        startActivity(i);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
