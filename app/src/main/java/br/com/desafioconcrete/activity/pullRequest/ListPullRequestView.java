package br.com.desafioconcrete.activity.pullRequest;

import java.util.List;

import br.com.desafioconcrete.base.BaseView;
import br.com.desafioconcrete.model.response.ResponseListPullRequest;
import br.com.desafioconcrete.model.response.ResponseListRepository;

/**
 * Created by Fernando on 30/08/2016.
 */
public interface ListPullRequestView extends BaseView{
    void onListPullRequestSucess(List<ResponseListPullRequest> listPullRequests);
    void onError(String error);
}
