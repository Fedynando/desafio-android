package br.com.desafioconcrete.activity.listRepository;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import br.com.desafioconcrete.R;
import br.com.desafioconcrete.base.BaseActivity;
import br.com.desafioconcrete.databinding.ActivityListBinding;
import br.com.desafioconcrete.endlessScroll.EndlessRecyclerViewScrollListener;
import br.com.desafioconcrete.model.response.ItemRepository;
import br.com.desafioconcrete.model.response.ResponseListRepository;
import br.com.desafioconcrete.activity.pullRequest.ListPullRequestActivity;

public class ListRepositoryActivity extends BaseActivity implements ListRepositoryView, AdapterListRepository.OnItemSelectedListener {

    private ListRepositoryPresenter mPresenter;
    private ActivityListBinding binding;
    private AdapterListRepository adapter;
    ResponseListRepository responseListRepository;
    private List<ItemRepository> mListItems;
    private final int firstPage = 1;
    public static final String BUNDLE_ITEM = "bundleItem";
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            initUI();
            initData();
    }

    @Override
    public void initUI() {
        super.initUI();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_list);
        this.mRecyclerView = binding.recyclerView;

        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void initData() {
        mListItems = new ArrayList<>();
        mPresenter = new ListRepositoryPresenterImpl(this, this);

        adapter = new AdapterListRepository(this, mListItems, this);
        mRecyclerView.setAdapter(adapter);

        mPresenter.listRepository(firstPage, true);
        loadItemListener();
        binding.swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pullRefresh();
            }
        });
    }

    private void pullRefresh(){
        binding.swipeContainer.setRefreshing(false);
        mListItems.clear();
        adapter.notifyDataSetChanged();
        mPresenter.listRepository(firstPage, true);
    }

    @Override
    public void onListRepositorySucess(ResponseListRepository responseListRepository) {
        this.responseListRepository = responseListRepository;

        int curSize = adapter.getItemCount();
        mListItems.addAll(responseListRepository.getItems());
        adapter.notifyItemRangeInserted(curSize, mListItems.size() - 1);
    }

    @Override
    public void onError(String error) {
        adapter.setLoad(false);
        toast(error);
    }

    @Override
    public void showProgress(ProgressType type) {
        onLoadingStart();
    }

    @Override
    public void hideProgress() {
        onLoadingFinish();
        if(binding.swipeContainer.isRefreshing()){
            binding.swipeContainer.setRefreshing(false);
        }
    }

    @Override
    public void onConnectionFailed() {
        adapter.setLoad(false);
        hideProgress();
        toast(getString(R.string.not_connect));
    }

    @Override
    public void onItemSelected(ItemRepository items) {
           putExtras(BUNDLE_ITEM, items, ListPullRequestActivity.class);
    }

    private void loadItemListener(){
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();

        mRecyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadMoreDataFromApi(page);
            }
        });
    }

    private void loadMoreDataFromApi(int page) {
        adapter.setLoad(true);
        mPresenter.listRepository(page, false);
    }
}
