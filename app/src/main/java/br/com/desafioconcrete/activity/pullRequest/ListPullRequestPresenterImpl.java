/*
 *
 *  * Copyright (C) 2014 Kanamobi.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package br.com.desafioconcrete.activity.pullRequest;

import android.app.Activity;
import android.content.Context;

import java.util.List;

import br.com.desafioconcrete.base.BaseView;
import br.com.desafioconcrete.model.response.ResponseListPullRequest;
import br.com.desafioconcrete.model.response.ResponseListRepository;
import br.com.desafioconcrete.utils.DeviceUtils;

public class ListPullRequestPresenterImpl implements ListPullRequestPresenter, OnListPullRequestFinishedListener {

    private ListPullRequestView baseView;
    private ListPullRequestInteractor interactor;
    private Context context;

    public ListPullRequestPresenterImpl(Context context, ListPullRequestView baseView) {
        this.baseView = baseView;
        this.context = context;
        this.interactor = new ListPullRequestInteractorImpl();
    }


    @Override
    public void onListPullRequestSuccess(final List<ResponseListPullRequest> listPullRequests) {
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                baseView.hideProgress();
                baseView.onListPullRequestSucess(listPullRequests);
            }
        });
    }


    @Override
    public void onError(final String errorMessage) {
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                baseView.hideProgress();
                baseView.onError(errorMessage);
            }
        });
    }

    @Override
    public void listPullRequest(String repository, String owner) {
        if(!DeviceUtils.isNetConnection(context)){
            baseView.onConnectionFailed();
            return;
        }

        baseView.showProgress(BaseView.ProgressType.PROGRESS_DIALOG);
        interactor.listPullRequest(context, repository, owner, this);
    }
}
