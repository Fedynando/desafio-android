package br.com.desafioconcrete.base;

/**
 * Created by fernandogomes on 10/06/16.
 */
public interface BaseView {

    enum ProgressType {
        PROGRESS_DIALOG,
        PROGRESS_VIEW,
        PROGRESS_SWIPE
    }

     void showProgress(ProgressType type);
     void hideProgress();
     void onConnectionFailed();
}
