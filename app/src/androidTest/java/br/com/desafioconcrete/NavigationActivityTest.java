package br.com.desafioconcrete;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.test.ActivityInstrumentationTestCase2;

import com.robotium.solo.Solo;

import br.com.desafioconcrete.activity.listRepository.ListRepositoryActivity;
import br.com.desafioconcrete.activity.pullRequest.ListPullRequestActivity;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class NavigationActivityTest extends ActivityInstrumentationTestCase2<ListRepositoryActivity>{

    private Activity activity;
    private Solo solo;

    public NavigationActivityTest() {
        super(ListRepositoryActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        activity = getActivity();
        solo = new Solo(getInstrumentation(), activity);
    }

    public void testListRepository(){

        final RecyclerView recyclerView = (RecyclerView) activity.findViewById(R.id.recyclerView);
        solo.clickInRecyclerView(1);
        if(solo.waitForActivity(ListPullRequestActivity.class, 2000)) {
            assertTrue(true);
        }else {
            assertTrue(false);
        }

        solo.sleep(3000);

        solo.setActivityOrientation(Solo.LANDSCAPE);
        solo.sleep(1000);
        solo.setActivityOrientation(Solo.PORTRAIT);
        solo.sleep(1000);


        solo.goBack();

        solo.setActivityOrientation(Solo.LANDSCAPE);
        solo.sleep(1000);
        solo.setActivityOrientation(Solo.PORTRAIT);
        solo.sleep(1000);

        assertNotNull(recyclerView.getAdapter());
        smoothScroll(recyclerView, recyclerView.getAdapter().getItemCount() - 1);
        solo.sleep(3000);
        smoothScroll(recyclerView, recyclerView.getAdapter().getItemCount() - 1);
        solo.sleep(3000);
        smoothScroll(recyclerView, 0);
        solo.sleep(3000);


    }

    @Override
    protected void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
    }

    private void smoothScroll(final RecyclerView recyclerView, final int position){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                recyclerView.smoothScrollToPosition(position);
            }
        });
    }

}