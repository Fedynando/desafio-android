package br.com.desafioconcrete;

import android.support.v7.widget.RecyclerView;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.widget.TextView;

import com.robotium.solo.Solo;

import br.com.desafioconcrete.activity.listRepository.ListRepositoryActivity;
import br.com.desafioconcrete.activity.pullRequest.ListPullRequestActivity;
import br.com.desafioconcrete.model.response.ItemRepository;


/**
 * Created by Fernando on 04/09/2016.
 */

public class UnitActivityTest extends ActivityInstrumentationTestCase2<ListRepositoryActivity> {

    ListRepositoryActivity activity;
    private Solo solo;

    public UnitActivityTest() {
        super(ListRepositoryActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        activity = getActivity();
        solo = new Solo(getInstrumentation(), activity);
    }



    public void testActivityRepository(){

            RecyclerView recyclerView = (RecyclerView) activity.findViewById(R.id.recyclerView);

            assertNotNull(recyclerView.getAdapter());
            int count = recyclerView.getAdapter().getItemCount();
            assertTrue(count > 1);

            TextView txtName = (TextView) solo.getView(R.id.txtName, 1);
            TextView txtDescription = (TextView) solo.getView(R.id.txtDescription, 1);
            TextView txtFork = (TextView) solo.getView(R.id.txtFork, 1);
            TextView txtStar = (TextView) solo.getView(R.id.txtStar, 1);
            TextView txtUserName = (TextView) solo.getView(R.id.txtUserName, 1);

            assertNotNull(txtName.getText().toString());
            assertNotNull(txtDescription.getText().toString());
            assertNotNull(txtFork.getText().toString());
            assertNotNull(txtStar.getText().toString());
            assertNotNull(txtUserName.getText().toString());


    }

    public void testPullActivity(){
        solo.clickInRecyclerView(0);

        boolean isNewActivity = solo.waitForActivity(ListPullRequestActivity.class, 5000);
        if(isNewActivity) {
            assertTrue(true);
        }else {
            assertTrue(false);
        }

        solo.sleep(2000);

        ListPullRequestActivity listPullRequestActivity = (ListPullRequestActivity) solo.getCurrentActivity();
        ItemRepository itemRepository = (ItemRepository) listPullRequestActivity.getIntent().getExtras().get(ListRepositoryActivity.BUNDLE_ITEM);
        assertNotNull(isNewActivity);

        assertNotNull(itemRepository.getName());
        assertNotNull(itemRepository.getOwner().getLogin());

        TextView txtTitle = (TextView) solo.getView(R.id.txtTitle, 1);
        TextView txtBody = (TextView) solo.getView(R.id.txtDescription, 1);

        assertNotNull(txtTitle.getText().toString());
        assertNotNull(txtBody.getText().toString());
    }


    @Override
    protected void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
    }
}